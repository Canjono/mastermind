import { GameObjects, Scene } from "phaser";

export class CodePeg extends GameObjects.Sprite {
  id: number;
  color: string;
}
