import { CodePeg } from "./codePeg";
import { Scene } from "phaser";

export class Board {
  image: Phaser.GameObjects.Image;
  codePegsContainer: Phaser.GameObjects.Container;

  initCodePegsContainer(codePegs: CodePeg[], scene: Scene): void {
    if (!codePegs || codePegs.length === 0) {
      return;
    }

    const boardBottomRight = this.image.getBottomRight();
    const containerPositionX = boardBottomRight.x + codePegs[0].displayWidth / 4;
    const containerPositionY = boardBottomRight.y - codePegs[0].displayHeight;
    this.codePegsContainer = scene.add.container(containerPositionX, containerPositionY);

    for (let i = 0; i < codePegs.length; i++) {
      const codePeg = codePegs[i];
      this.codePegsContainer.add(codePeg);
      codePeg.setPosition(0, i * -codePeg.displayHeight);
    }

    console.log(this.codePegsContainer);
  }
}
