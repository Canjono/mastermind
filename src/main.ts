import * as Phaser from 'phaser';
import { GameScene } from './scenes/gameScene'

const gameConfig: Phaser.Types.Core.GameConfig = {
  title: 'Mastermind',

  type: Phaser.AUTO,

  scale: {
    width: window.innerWidth,
    height: window.innerHeight,
  },

  physics: {
    default: 'arcade',
    arcade: {
      debug: true,
    },
  },

  parent: 'game',
  backgroundColor: '#f1f1f1',

  scene: GameScene
};

export const game = new Phaser.Game(gameConfig);