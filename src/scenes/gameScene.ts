import { CodePeg } from "../models/codePeg";
import { Board } from "../models/board";

const sceneConfig: Phaser.Types.Scenes.SettingsConfig = {
  active: false,
  visible: false,
  key: 'Game',
};

export class GameScene extends Phaser.Scene {
  private board: Board;
  private codePegs: CodePeg[];
  private selectedCodePeg: CodePeg;

  constructor() {
    super(sceneConfig);
  }

  public create(): void {
    this.initBoard();
    this.initCodePegs();
    this.placeCodePegs();
  }

  public update(): void {
    // TODO
  }

  public preload(): void {
    this.load.image('board', 'assets/sprites/Boardwithshadow.png');
    this.load.image('yellowPeg', 'assets/sprites/yellow-peg.png');
    this.load.image('turquoisePeg', 'assets/sprites/turquoise-peg.png');
    this.load.image('bluePeg', 'assets/sprites/blue-peg.png');
    this.load.image('redPeg', 'assets/sprites/red-peg.png');
    this.load.image('greenPeg', 'assets/sprites/green-peg.png');
    this.load.image('purplePeg', 'assets/sprites/purple-peg.png');
  }

  private initBoard(): void {
    const canvasHeight = this.sys.canvas.height;
    const canvasWidth = this.sys.canvas.width;
    this.board = new Board();
    this.board.image = this.add.image(canvasWidth / 2, canvasHeight / 2, 'board')
    this.board.image.setDisplaySize(canvasHeight / 1.8, canvasHeight);
  }

  private initCodePegs(): void {
    this.codePegs = [];
    const codePegColors = [
      'yellow',
      'blue',
      'red',
      'green',
      'turquoise',
      'purple'
    ];

    for (let i = 0; i < codePegColors.length; i++) {
      const color = codePegColors[i];
      const codePeg = this.createCodePeg(color, i);
      this.codePegs.push(codePeg);
    }
  }

  private createCodePeg(color: string, id: number): CodePeg {
    const codePeg = this.add.sprite(0, 0, `${color}Peg`) as CodePeg;
    codePeg.setInteractive();
    codePeg.id = id;
    codePeg.color = color;
    const codePegScale = this.board.image.displayWidth * 0.0005;
    codePeg.setScale(codePegScale, codePegScale);

    codePeg.on('pointerdown', pointer => {
      console.log('pick up ' + codePeg.color);
    });

    return codePeg;
  }

  private placeCodePegs(): void {
    this.board.initCodePegsContainer(this.codePegs, this);
  }
}
